import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author Divjot Chawla
 * chawladi
 * 991505770
 * 
 * Test Class That contains all the methods
 */

public class PasswordValidatorTest {

	
	@Test
	public void testIsValidPasswordBoundaryIn() {
		boolean isValidPassword= PasswordValidator.isValidPassword("Divjot010");
		assertTrue("Invalid Passowrd", isValidPassword == true);
	}
	@Test(expected=NumberFormatException.class)
	public void testIsValidPasswordException() {
		boolean isValidPassword= PasswordValidator.isValidPassword("abc123");
		fail("Invalid Password!");
	}
	
	
	@Test
	public void testIsValidPasswordRegular() {
		boolean isValidPassword= PasswordValidator.isValidPassword("abcabc10b");
	assertTrue("Invalid Password", isValidPassword==true);
	}
	
	@Test(expected=NumberFormatException.class)
	public void testIsValidPasswordBoundaryOut() {
		boolean isValidPassword= PasswordValidator.isValidPassword("HelloWorld1");
		fail("Invalid Password!");
	}
}
























