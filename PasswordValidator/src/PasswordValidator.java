/**
 * 
 * @author Divjot Chawla
 * chawladi
 * 991505770
 * 
 * This class contains method that validates the string input.
 *
 */
public class PasswordValidator {

public static boolean isValidPassword( String password) {
	int countDigits=0;
	
	for(int i=0; i< password.length(); i++) {
		if(Character.isDigit(password.charAt(i))) countDigits++;
		
	}
	if((password.contains(" ")))
    throw new NumberFormatException("Invalid Password");
	
	else if(password.length()<8)
	throw new NumberFormatException("Password should be atleast 8 characters long.");

	else if ((countDigits == 0) ||(countDigits== 1))
		throw new NumberFormatException("Password needs to have 2 digits at least");
	
	return true;
}

























} 
